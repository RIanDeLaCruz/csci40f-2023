from django.apps import AppConfig


class AppledappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'appledapp'
